@extends('layouts.app')
@section('content')
<body class="news-article">
<div class="content">
   @include('layouts.main_menu')
    <div class="container">
        <div class="personal-area__wrapper">
            <div class="news-article__wrapper">
                @if($thing->type == 1)
                <h1 class="news-article__title">Пропажа</h1>
                @else()
                <h1 class="news-article__title">Находка</h1>
                @endif
                <div class="news-article__date">{{ $thing->created_at }}</div>
                <div class="news-article__date">{{ $thing->user['name'] }}, {{ $thing->user['course'] }} курс {{ $thing->user->group->name }}</div>
                <div class="news-article__date">{{ $thing->phone }}</div>
                <div class="news-article__date">{{ $thing->second_phone }}</div>
                <h6 class="personal-area__name">{{ $thing->disc }}</h6>
                @if($thing->photo)
                <div class="news-article__img--wrapper">
                    <img class="news-article__img" src="{{ asset('/storage/' . $thing->photo) }}">
                </div>
                @endif
            </div>
        </div>
    </div>
</div>
@endsection
