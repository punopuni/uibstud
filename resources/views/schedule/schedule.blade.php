@extends('layouts.app')
@section('content')
<body class="schedule">
<div class="content">
    @include('layouts.main_menu')
    <div class="container">
        <div class="employees__title">Расписание</div>
        <div class="links">
            <div class="links__wrapper">
                <a class="links__item active" href="#">Семестровое</a>
{{--                <a class="links__item" href="#">Экзамиционное</a>--}}
            </div>
        </div>
        <div class="schedule__wrapper">
            <div class="map__table">
                <div class="schedule__title">Понедельник</div>
                <div class="map__table--items">
                    <div class="map__table--time">Время</div>
                    <div class="map__table--number">Аудитория</div>
                    <div class="map__table--desc">Предмет</div>
                    <div class="map__table--author">Преподователь</div>
                </div>
                @foreach($schedule as $item)
                    @if($item->day == 1)
                <div class="map__table--items">
                    <div class="map__table--time">{{ $item->time }}</div>
                    <div class="map__table--number">{{ $item->audience }}</div>
                    <div class="map__table--desc">{{ $item->subject->name }}</div>
                    <div class="map__table--author">{{ $item->teacher }}</div>
                </div>
                    @endif
                @endforeach
               </div>
            <div class="map__table">
                <div class="schedule__title">Вторник</div>
                <div class="map__table--items">
                    <div class="map__table--time">Время</div>
                    <div class="map__table--number">Аудитория</div>
                    <div class="map__table--desc">Предмет</div>
                    <div class="map__table--author">Преподователь</div>
                </div>
                @foreach($schedule as $item)
                    @if($item->day == 2)
                        <div class="map__table--items">
                            <div class="map__table--time">{{ $item->time }}</div>
                            <div class="map__table--number">{{ $item->audience }}</div>
                            <div class="map__table--desc">{{ $item->subject->name }}</div>
                            <div class="map__table--author">{{ $item->teacher }}</div>
                        </div>
                    @endif
                @endforeach
            </div>
            <div class="map__table">
                <div class="schedule__title">Среда</div>
                <div class="map__table--items">
                    <div class="map__table--time">Время</div>
                    <div class="map__table--number">Аудитория</div>
                    <div class="map__table--desc">Предмет</div>
                    <div class="map__table--author">Преподователь</div>
                </div>
                @foreach($schedule as $item)
                    @if($item->day == 3)
                        <div class="map__table--items">
                            <div class="map__table--time">{{ $item->time }}</div>
                            <div class="map__table--number">{{ $item->audience }}</div>
                            <div class="map__table--desc">{{ $item->subject->name }}</div>
                            <div class="map__table--author">{{ $item->teacher }}</div>
                        </div>
                    @endif
                @endforeach
            </div>
                <div class="map__table">
                    <div class="schedule__title">Четверг</div>
                    <div class="map__table--items">
                        <div class="map__table--time">Время</div>
                        <div class="map__table--number">Аудитория</div>
                        <div class="map__table--desc">Предмет</div>
                        <div class="map__table--author">Преподователь</div>
                    </div>
                    @foreach($schedule as $item)
                        @if($item->day == 4)
                            <div class="map__table--items">
                                <div class="map__table--time">{{ $item->time }}</div>
                                <div class="map__table--number">{{ $item->audience }}</div>
                                <div class="map__table--desc">{{ $item->subject->name }}</div>
                                <div class="map__table--author">{{ $item->teacher }}</div>
                            </div>
                        @endif
                    @endforeach
                </div>
            <div class="map__table">
                <div class="schedule__title">Пятница</div>
                <div class="map__table--items">
                    <div class="map__table--time">Время</div>
                    <div class="map__table--number">Аудитория</div>
                    <div class="map__table--desc">Предмет</div>
                    <div class="map__table--author">Преподователь</div>
                </div>
                @foreach($schedule as $item)
                    @if($item->day == 5)
                        <div class="map__table--items">
                            <div class="map__table--time">{{ $item->time }}</div>
                            <div class="map__table--number">{{ $item->audience }}</div>
                            <div class="map__table--desc">{{ $item->subject->name }}</div>
                            <div class="map__table--author">{{ $item->teacher }}</div>
                        </div>
                    @endif
                @endforeach
            </div>
            <div class="map__table">
                <div class="schedule__title">Суббота</div>
                <div class="map__table--items">
                    <div class="map__table--time">Время</div>
                    <div class="map__table--number">Аудитория</div>
                    <div class="map__table--desc">Предмет</div>
                    <div class="map__table--author">Преподователь</div>
                </div>
                @foreach($schedule as $item)
                    @if($item->day == 6)
                        <div class="map__table--items">
                            <div class="map__table--time">{{ $item->time }}</div>
                            <div class="map__table--number">{{ $item->audience }}</div>
                            <div class="map__table--desc">{{ $item->subject->name }}</div>
                            <div class="map__table--author">{{ $item->teacher }}</div>
                        </div>
                    @endif
                @endforeach
            </div>
        </div>
    </div>
</div>
@endsection
