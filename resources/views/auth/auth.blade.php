@extends('layouts.app')
@section('content')
<body class="authorization">
    <div class="content">
        <nav class="main-menu">
            <div class="main-menu__wrapper">
                <ul>
                    <li><a href="index.html"><i class="fa fa-2x fas fa-user-graduate"></i><span class="nav-text">Личный кабинет</span></a></li>
                    <li class="has-subnav"><a href="employees.html"><i class="fa fa-2x fas fa-id-card-alt"></i><span class="nav-text">Преподователи</span></a></li>
                    <li class="has-subnav"><a href="map.html"><i class="fa fa-2x fas fa-map-marked-alt"></i><span class="nav-text">Карта ВУЗа</span></a></li>
                    <li class="has-subnav"><a href="news.html"><i class="fa fa-2x far fa-newspaper"></i><span class="nav-text">Новости</span></a></li>
                    <li><a href="#"><i class="fa fa-2x fas fa-book-open"></i><span class="nav-text">Расписание</span></a></li>
                    <li><a href="networking.html"><i class="fa fa-2x fas fa-users"></i><span class="nav-text">Нетворкинг</span></a></li>
                    <li><a href="things.html"><i class="fa fa-2x fas fa-search"></i><span class="nav-text">Потеряшка</span></a></li>
                </ul>
            </div>
        </nav>
        <div class="authorization__bg">
            <div class="authorization__wrapper">
                <div class="container">
                    <h1>Добро пожаловать в UIBstud!</h1>
                    <form class="authorization__form" action="/auth" method="post">
                        @csrf
                        <input type="text" placeholder="Логин" name="barcode">
                        <input type="password" placeholder="Пароль" name="password">
                        <button id="login-button" type="submit">Войти</button>
                        @if(isset($error))
                            <p style="color: red">{{ $error }}</p>
                        @endif
                    </form>
                </div>
            </div>
            <ul class="authorization__bubbles">
                <li></li>
                <li></li>
                <li></li>
                <li></li>
                <li></li>
                <li></li>
                <li></li>
                <li></li>
                <li></li>
                <li></li>
            </ul>
        </div>
    </div>
@endsection
