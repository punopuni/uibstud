@if ($paginator->hasPages())
    <nav class="nav-pagination" aria-label="Page navigation">
        <ul class="pagination">
            {{-- Previous Page Link --}}
            @if ($paginator->onFirstPage())
                <li class="pagination__item" aria-disabled="true" aria-label="@lang('pagination.previous')">
                    <a class="pagination__link" href="#">
                        <i class="fas fa-angle-left"></i>
                    </a>
                </li>
            @else
                <li class="page-item">
                    <a class="pagination__link" href="{{ $paginator->previousPageUrl() }}" rel="prev" aria-label="@lang('pagination.previous')">
                        <i class="fas fa-angle-left"></i>
                    </a>
                </li>
            @endif

            {{-- Pagination Elements --}}
            @foreach ($elements as $element)
                {{-- "Three Dots" Separator --}}
                @if (is_string($element))
                    <li class="pagination__item active" aria-disabled="true"><a class="pagination__link">{{ $element }}</a></li>
                @endif

                {{-- Array Of Links --}}
                @if (is_array($element))
                    @foreach ($element as $page => $url)
                        @if ($page == $paginator->currentPage())
                            <li class="pagination__item active" aria-current="page"><a class="pagination__link">{{ $page }}</a></li>
                        @else
                            <li class="pagination__item"><a class="pagination__link" href="{{ $url }}">{{ $page }}</a></li>
                        @endif
                    @endforeach
                @endif
            @endforeach

            {{-- Next Page Link --}}
            @if ($paginator->hasMorePages())
                <li class="pagination__item">
                    <a class="pagination__link" href="{{ $paginator->nextPageUrl() }}" rel="next" aria-label="@lang('pagination.next')"><i class="fas fa-angle-right"></i></a>
                </li>
            @else
                <li class="pagination__item" aria-disabled="true" aria-label="@lang('pagination.next')">
                    <a class="pagination__link"><i class="fas fa-angle-right"></i></a>
                </li>
            @endif
        </ul>
    </nav>
@endif
