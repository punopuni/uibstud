@extends('layouts.app')
@section('content')
    <body class="personal-area">
    <div class="content">
        @include('layouts.main_menu')
        <div class="container">
            <div class="personal-area__wrapper">
                <div class="personal-area__item">
                    <div class="personal-area__ava--wrapper">
                        <div class="personal-area__ava" style="background-image: url({{ '/storage/'.$user->avatar }})"></div>
                        <a class="personal-area__ava--edit" type="file" href="#">
                            <form enctype="multipart/form-data" method="post" action="/upload_photo">
                                @csrf
                                <input class="personal-area__ava--edit" type="file" name="photo">
                                <button class="btn btn--default" type="submit">Отправить</button>
                            </form>
                        </a>
                    </div>
                    <div class="personal-area__text">
                        <h6 class="personal-area__name">{{ $user->name }}</h6>
                        <div class="personal-area__info">
                            <p>Группа: {{ $user->group->name }}</p>
                            <p>Форма: {{ $user->form }}</p>
                            <p>Уровень: {{ $user->level }}</p>
                            <p>Специальность или ОП: {{ $user->spec }}</p>
                            <p>Штрих-код: {{ $user->barcode }}</p>
                            <p>Курс: {{ $user->course }}</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
