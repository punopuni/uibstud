<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;

class AuthController extends Controller
{

    //Блок вывода вьюшки входа
    public function index()
    {
        return view('auth.auth');
    }

    // Блок авторизации по баркоду
    public function authorization(Request $request)
    {
        $data = $request->all();
        $check = Auth::attempt([
            'barcode'  => $data['barcode'],
            'password' => $data['password'],
        ]);

        if($check){
            $user = User::where('barcode', $data['barcode'])->first();
            Auth::login($user);
            return redirect()->route('personal_area');
        } else {
            return view('auth.auth', ['error' => 'Неверный логин или пароль']);
        }
    }

    // Блок выхода из аккаунта
    public function logout()
    {
        Session::flush();
        Auth::logout();
        return redirect('/');
    }
}
