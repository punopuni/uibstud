<?php

namespace App\Http\Controllers;

use App\Think;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;

class ThinkController extends Controller
{
    public function index()
    {
        $vars = [
            'things'     => DB::table('thinks')->latest('created_at')->paginate(4),
        ];
        return view('things.things', $vars);
    }

    public function article_index($id)
    {
        $vars = [
            'thing'  => Think::find($id),
        ];
        return view('things.thing', $vars);
    }

    public function category_index($id)
    {
        $vars = [
          'things'   => Think::where('type', $id)->latest('created_at')->paginate(4),
          'id'      => $id,
        ];
        return view('things.category_thing', $vars);
    }

    public function create()
    {
        return view('things.things_form');
    }

    public function store(Request $request)
    {
        $thing = new Think();
        $data = $request->all();

        $user = Auth::user();

        $thing->user_id = $user->id;

        $thing->phone = $data['phone'];
        $thing->second_phone = $data['sec_phone'];
        $thing->disc = $data['disc'];
        if($thing->photo) $thing->photo = $this->upload_photo($request);
        $thing->type = $data['type'];

        $thing->save();
        return redirect('/things');
    }

    public function upload_photo($data)
    {
        $path = Storage::putFile('public/things', $data->file('photo'));
        $path_detail = explode('/', $path);
        return $path_detail[1] . '/' . $path_detail[2];
    }
}
