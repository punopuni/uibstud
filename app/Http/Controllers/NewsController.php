<?php

namespace App\Http\Controllers;

use App\News;
use App\NewsCategory;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;

class NewsController extends Controller
{
    public function index()
    {
        $type = $this->get_type_of_url();
        $vars = [
            'news'       => News::where('type', $type)->paginate(3),
            'categories' => NewsCategory::where('type', $type)->get(),
            'type'       => $type,
        ];
        return view('news.news', $vars);
    }

    public function category_index($id)
    {
        $type = $this->get_type_of_url();
        $vars = [
            'news'          => NewsCategory::find($id)->news()->paginate(3),
            'categories'    => NewsCategory::where('type', $type)->get(),
            'id'            => $id,
            'type'          => $type,
        ];
        return view('news.category_news', $vars);
    }

    //Определение типа для правильных данных
    public function get_type_of_url()
    {
        $route = explode('/', url()->current());
        if($route[3] == 'news') return $type = 1;
        else return $type = 2;
    }

    public function article($id)
    {
        $article = News::find($id);
        $vars = [
            'article'   => $article,
            'category'  => $article->newscategory,
            'user'      => $article->user,
            'type'      => $article->type,
        ];
        return view('news.article', $vars);
    }

    public function network_form()
    {
        $vars = [
            'categories'  => NewsCategory::where('type', 2)->get(),
        ];
        return view('news.network_form', $vars);
    }

    public function network_save(Request $request)
    {
        $data = $request->all();
        $news = new News;

        $news->name = $data['name'];
        $news->disc = $data['disc'];
        $news->text = $data['text'];
        $news->author_id = Auth::user()->id;
        $news->news_category_id = $data['category'];
        $news->type = 2;

        if($data['photo']) $news->photo = $this->upload_photo($request);

        $news->save();
        return redirect('/article/' . $news->id);
    }

    public function upload_photo($data)
    {
        $path = Storage::putFile('public/network', $data->file('photo'));
        $path_detail = explode('/', $path);
        return $path_detail[1] . '/' . $path_detail[2];
    }
}
