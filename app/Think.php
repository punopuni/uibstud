<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Think extends Model
{
    protected $fillable = ['phone', 'second_phone', 'disc', 'photo', 'type'];

    public function user()
    {
        return $this->belongsTo('App\User', 'user_id');
    }
}
