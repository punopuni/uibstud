<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Subject extends Model
{
    protected $fillable = ['name'];

    public function schedule()
    {
        return $this->hasMany('App\Schedule', 'subject_id');
    }
}
