<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Map extends Model
{
    protected $fillable = ['number', 'desc', 'floor_id'];

    public function floor()
    {
        $this->hasMany('App\Floor');
    }
}
