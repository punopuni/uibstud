<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Teacher extends Model
{
    protected $fillable = [
      'name', 'disc', 'about', 'photo', 'type',
    ];

    protected $table = 'teacher';

    public function teachercategory()
    {
        return $this->belongsTo('App\TeacherCategory');
    }
}
