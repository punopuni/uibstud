<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TeacherCategory extends Model
{
    protected $fillable = ['name'];

    public function teacher()
    {
        return $this->hasMany('App\Teacher', 'teacher_category_id');
    }
}
