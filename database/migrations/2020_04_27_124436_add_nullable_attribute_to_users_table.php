<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddNullableAttributeToUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->string('group')->nullable()->change();
            $table->string('form')->nullable()->change();
            $table->string('level')->nullable()->change();
            $table->string('spec')->nullable()->change();
            $table->integer('barcode')->nullable()->change();
            $table->integer('course')->nullable()->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->string('group')->change();
            $table->string('form')->change();
            $table->string('level')->change();
            $table->string('spec')->change();
            $table->integer('barcode')->change();
            $table->integer('course')->change();
        });
    }
}
