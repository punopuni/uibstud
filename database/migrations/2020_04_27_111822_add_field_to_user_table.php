<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddFieldToUserTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->string('group');
            $table->string('form');
            $table->string('level');
            $table->string('spec');
            $table->integer('barcode');
            $table->integer('course');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->dropColumn('group');
            $table->dropColumn('form');
            $table->dropColumn('level');
            $table->dropColumn('spec');
            $table->dropColumn('barcode');
            $table->dropColumn('course');
        });
    }
}
